// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var rawSchema = new Schema({
	date: Date,
	article: String,
	words: {},
	sentences: {}
});

// the schema is useless so far
// we need to create a model using it
var Raw = mongoose.model('raw', rawSchema);

// make this available to our users in our Node applications
module.exports = Raw;