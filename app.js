var extractor = require('unfluff');
var request = require('request');
var cheerio = require("cheerio");
var natural = require('natural');
var stem = require('stem-porter');
var fs = require('fs');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/botthatwrites');
var Raw = require('./models/raw');

var express = require('express');
var app = express();
var bodyParser = require('body-parser');


//webserver
// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

// use res.render to load up an ejs view file

// index page 
app.get('/', function (req, res) {
	res.render('url');
});

app.post('/url', function (req, res) {
	calculate(req.body.url);
});

app.listen(3000);
console.log('started @ :3000');

function calculate(url) {
	var time = Date.now();

	var url = url; //"http://www.sciencealert.com/google-s-quantum-computer-is-helping-us-understand-quantum-physics";

	//console.log('[1] Started - ' + (Date.now() - time));

	request(url, function (error, response, body) {
		if (error) {
			console.log('error:', error); // Print the error if one occurred
		}

		//console.log('[2] Article Recieved - ' + (Date.now() - time));
		time = Date.now();

		//console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
		//console.log('body:', body); // Print the HTML for the Google homepage.

		var data = extractor(body);
		var article = data.text;

		article = article.replace(/(?:\r\n|\r|\n)/g, '');

		//console.log('[3] Extracted - ' + (Date.now() - time));
		time = Date.now();

		var tokenizer = new natural.WordTokenizer();
		var tokens = tokenizer.tokenize(article);

		var stems = [];

		for (token in tokens) {
			stems.push(stem(tokens[token]));
		}

		//console.log('[4] Stemmed - ' + (Date.now() - time));
		time = Date.now();

		var c = calculateComplexity();
		//console.log('[5] Calculated Complexity - ' + (Date.now() - time));
		time = Date.now();

		var words = [];

		for (token in tokens) {
			//console.log(stem(tokens[token]));
			occurenceOrig = countInArray(tokens, tokens[token]);
			occurenceStem = countInArray(stems, stems[token]);
			occurence = Math.floor((occurenceOrig + occurenceStem) / 2);
			complexity = wordComplex(c, tokens[token].toLowerCase(), "complexity");
			diff = 1;
			switch (tokens[token].length) {
			case 1:
				diff = 0.2;
				break;
			case 2:
				diff = 0.5;
				break;
			case 3:
				diff = 0.7;
				break;
			default:
				diff = 1;
				break;
			}

			words.push({
				"word": tokens[token],
				"stemmed": stems[token],
				"occurenceOrig": occurenceOrig,
				"occurenceStem": occurenceStem,
				"occurence": occurence,
				"complexity": complexity,
				"difficulty": diff,
				"total": occurence * complexity * diff
			});
		}

		//console.log('[6] Calculated Totals - ' + (Date.now() - time));
		time = Date.now();

		var sorted = dedupe(words);
		sorted = sorted.sort(dynamicSort("-total"));
		//console.log(showSomeArray(sorted, 5));
		//console.log('[7] Sorted - ' + (Date.now() - time));
		time = Date.now();

		//get sentences by checking for capital letters and stops
		var sentences = [];
		var regex = new RegExp(".{0,}?(?:\\.|!|\\?)(?:(?=\\ [A-Z0-9])|$)", "g");
		var match;
		while ((match = regex.exec(article)) != null) {
			// javascript RegExp has a bug when the match has length 0
			if (match.index === regex.lastIndex) {
				++regex.lastIndex;
			}
			sentences.push(match[0]);
		}

		//console.log(sentences);
		//console.log('[7] Sentences discovered - ' + (Date.now() - time));
		time = Date.now();

		//create score of sentences char/score
		var sen = [];
		for (i in sentences) {
			var score = 0;
			var sentence = sentences[i];
			var word = tokenizer.tokenize(sentence);
			for (j in word) {
				score += (wordComplex(sorted, word[j], "total"));
			}
			sen.push({
				"order": parseInt(i),
				"sentence": sentence,
				"score": score / (word.length)
			});
		}
		//sen = sen.sort(dynamicSort("-score"));
		//sen = sen.slice(0, 9); //get top 10 sentences
		//sen = sen.sort(dynamicSort("order")); //in chronologoical order

		//console.log('[8] Sentences scored & sorted - ' + (Date.now() - time));
		time = Date.now();
		//console.log(sen);

		var d = new Raw({
			date: new Date(),
			article: article,
			words: sorted,
			sentences: sen
		});

		d.save(function (err) {
			if (err) throw err;
		});
	});

	function countInArray(array, what) {
		var count = 0;
		for (var i = 0; i < array.length; i++) {
			if (array[i] === what) {
				count++;
			}
		}
		return count;
	}

	function dynamicSort(property) {
		var sortOrder = 1;
		if (property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}
		return function (a, b) {
			var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			return result * sortOrder;
		}
	}

	function dedupe(arr) {
		return arr.reduce(function (p, c) {

			// create an identifying id from the object values
			var id = [c.original, c.occurence].join('|');

			// if the id is not found in the temp array
			// add the object to the output array
			// and add the key to the temp array
			if (p.temp.indexOf(id) === -1) {
				p.out.push(c);
				p.temp.push(id);
			}
			return p;

			// return the deduped array
		}, {
			temp: [],
			out: []
		}).out;
	}

	function wordComplex(data, word, value) {
		try {
			return ((data.find(x => x.word == word)[value]) * 0.75);
		} catch (err) {
			return 1
		}
	}

	function calculateComplexity() {
		var data = fs.readFileSync('google-10000-english-usa.txt', 'utf8');

		var words = data.split("\r\n");
		words = words.reverse();

		var c = [];
		for (i in words) {
			c.push({
				"word": words[i],
				"complexity": ((words.length - i) / words.length)
			});
		}

		return c;
	}

	function showSomeArray(arr, amount) {
		return (arr.slice(0, amount));
	}
}